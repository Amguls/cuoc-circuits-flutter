import 'package:cuoc_circuits_flutter/colours.dart';
import 'package:cuoc_circuits_flutter/utilities.dart';
import 'package:cuoc_circuits_flutter/workout_build_plan.dart';
import 'package:cuoc_circuits_flutter/workout_plan.dart';
import 'package:cuoc_circuits_flutter/workout_preview.dart';
import 'package:flutter/material.dart';

class WorkoutSelectPage extends StatefulWidget {
  WorkoutSelectPage({Key key, @required this.workoutDuration})
      : super(key: key);

  final Duration workoutDuration;

  @override
  _WorkoutSelectState createState() => _WorkoutSelectState();
}

class _WorkoutSelectState extends State<WorkoutSelectPage> {
  List<WorkoutPlan> plans;
  WorkoutPlan selectedPlan;

  @override
  void initState() {
    plans = [
      WorkoutPlan.standardWorkout(widget.workoutDuration),
      WorkoutPlan.pyramidWorkout(widget.workoutDuration)
    ];
    selectedPlan = plans.first;
    super.initState();
  }

  void displayInfo(WorkoutPlan plan) {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            backgroundColor: Theme.of(context).backgroundColor,
            child: ListView(
                children: plan.activities
                    .where((a) => a.type != ActiveState.TRANSITION)
                    .map((activity) => Card(
                          color: (activity.type == ActiveState.EXERCISE)
                              ? cuocColor[900]
                              : cuocColor[600],
                          child: ListTile(
                            title: Text(Utilities.getEnumName(activity.type)),
                            trailing: Text(
                                activity.duration.inSeconds.toString() + 's'),
                          ),
                        ))
                    .toList()),
          );
        });
  }

  Widget buildSelection(context, WorkoutPlan plan) {
    return GestureDetector(
        onTap: () => setState(() => selectedPlan = plan),
        child: Card(
          shape: RoundedRectangleBorder(
              side: BorderSide(
                  color: (plan == selectedPlan)
                      ? cambridgeColor[900]
                      : cuocColor[900],
                  width: 2.0),
              borderRadius: BorderRadius.circular(4.0)),
          color: cuocColor[900],
          child: ListTile(
            title: Text(plan.name),
            trailing: IconButton(
              icon: Icon(Icons.info_outline, color: cambridgeColor[900]),
              onPressed: () => displayInfo(plan),
            ),
          ),
        ));
  }

  Widget buildNewSelection(context) {
    return GestureDetector(
        onTap: () => Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => WorkoutPlanManagePage())),
        child: Card(
          color: cuocColor[900],
          child: ListTile(
            title: Center(child: Text("Create new workout...")),
          ),
        ));
  }

  @override
  Widget build(context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              child: Text(
                "Select Workout:",
                style: Theme.of(context).primaryTextTheme.headline5,
              ),
            ),
            Expanded(
              child: ListView(
                children:
                    plans.map((plan) => buildSelection(context, plan)).toList(),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[/*
                    TextButton(
                        text: "Create Custom Workout",
                        onPress: () => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) =>
                                    WorkoutPlanManagePage()))),*/
                    TextButton(
                        text: "Continue",
                        onPress: () => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => WorkoutConfirmPage(
                                    workoutTime: widget.workoutDuration,
                                    workoutPlan: selectedPlan)))),
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}
