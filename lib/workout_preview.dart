import 'package:cuoc_circuits_flutter/exercises.dart';
import 'package:cuoc_circuits_flutter/utilities.dart';
import 'package:cuoc_circuits_flutter/workout_plan.dart';
import 'package:flutter/material.dart';

import 'package:cuoc_circuits_flutter/workout_active.dart';

class WorkoutConfirmPage extends StatefulWidget {
  WorkoutConfirmPage(
      {Key key, @required this.workoutTime, @required this.workoutPlan})
      : super(key: key);

  final Duration workoutTime;
  final WorkoutPlan workoutPlan;

  @override
  _WorkoutConfirmPageState createState() => _WorkoutConfirmPageState();
}

class _WorkoutConfirmPageState extends State<WorkoutConfirmPage> {
  List<Exercise> selectedExercises = [];
  Future<void> completedSelection;

  Future<void> selectExercises() async {
    widget.workoutPlan.reset();
    return widget.workoutPlan
        .selectExercises(widget.workoutTime)
        .then((list) => setState(() {
              selectedExercises = list;
            }));
  }

  @override
  void initState() {
    super.initState();
    completedSelection = selectExercises();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 80,
              child: Center(
                child: Text(
                  "Suggested Exercises:",
                  style: Theme.of(context).primaryTextTheme.headline5,
                ),
              ),
            ),
            Flexible(
              child: FutureBuilder<void>(
                  future: completedSelection,
                  builder:
                      (BuildContext context, AsyncSnapshot<void> snapshot) {
                    return ExerciseList(
                        values: selectedExercises,
                        builder: (_, exercise) {
                          return ListTile(
                            title: Text(
                              exercise.name,
                            ),
                          );
                        },
                        snapshot: snapshot);
                  }),
            ),
            Text('${widget.workoutPlan.numExercisesInSet} exercises',
                style: Theme.of(context).primaryTextTheme.bodyText2),
            Text('${widget.workoutPlan.numSets} sets',
                style: Theme.of(context).primaryTextTheme.bodyText2),
            Text(
                'Total Time: ${Utilities.formatTime(widget.workoutPlan.totalDuration)}',
                style: Theme.of(context).primaryTextTheme.bodyText2),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[/*
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 1),
                      child: TextButton(
                          text: "Manual select",
                          onPress: Navigator.of(context).pop),
                    ),
                  ),*/
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 4),
                      child: TextButton(
                          text: "Regenerate",
                          onPress: () {
                            setState(() {
                               completedSelection = selectExercises();
                            });
                          }),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 4),
                      child: TextButton(
                          text: "Confirm",
                          onPress: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => WorkoutActivePage(
                                      workoutPlan: widget.workoutPlan,
                                      exercises: selectedExercises,
                                    )));
                          }),
                    ),
                  ),
                ]),
          ],
        ),
      ),
    );
  }
}
