import 'package:cuoc_circuits_flutter/exercises.dart';
import 'package:cuoc_circuits_flutter/utilities.dart';
import 'package:flutter/material.dart';

import 'package:cuoc_circuits_flutter/appdata.dart';

class EnumRadio<T> extends StatelessWidget {
  EnumRadio({@required this.enumValues, @required this.value, this.onChange});

  final List<T> enumValues;
  final T value;
  final void Function(T) onChange;

  @override
  Widget build(context) {
    List<Widget> rowElements = List<Widget>();

    for (T enumValue in enumValues) {
      rowElements.add(
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(Utilities.getEnumName(enumValue)),
            Radio(
              groupValue: value,
              value: enumValue,
              onChanged: onChange,
            ),
          ],
        ),
      );
    }

    return Row(
        mainAxisAlignment: MainAxisAlignment.center, children: rowElements);
  }
}

class EnumChecks<T> extends StatelessWidget {
  EnumChecks(
      {@required this.enumValues,
      @required this.values,
      this.disabledValue,
      this.onChange});

  final List<T> enumValues;
  final Set<T> values;
  final T disabledValue;
  final void Function(bool, T) onChange;

  @override
  Widget build(context) {
    List<Widget> rowElements = List<Widget>();

    for (T value in enumValues) {
      rowElements.add(
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(Utilities.getEnumName(value)),
            Checkbox(
              value: values.contains(value),
              onChanged: value == disabledValue
                  ? null
                  : (newVal) => onChange(newVal, value),
            ),
          ],
        ),
      );
    }
    return Row(
        mainAxisAlignment: MainAxisAlignment.center, children: rowElements);
  }
}

class DiscardChangesConfirmDialog extends StatelessWidget {
  DiscardChangesConfirmDialog(
      {@required this.exercise,
      @required this.creating,
      @required this.modifiedExercise});

  final bool creating;
  final Exercise exercise;
  final Exercise modifiedExercise;

  @override
  Widget build(context) {
    return AlertDialog(
      title: Text("Confirm"),
      content: creating
          ? Text("Discard new exercise \"${modifiedExercise.name}\"?")
          : Text("Discard changes to \"${exercise.name}\"?"),
      actions: <Widget>[
        TextButton(
          text: "Discard changes",
          onPress: () => Navigator.of(context).pop(true),
        ),
        TextButton(
          text: "Cancel",
          onPress: () => Navigator.of(context).pop(false),
        ),
      ],
    );
  }
}

class ExerciseModifyModal extends StatefulWidget {
  ExerciseModifyModal(this.exercise);

  final Exercise exercise;

  @override
  _ExerciseModifyModalState createState() => _ExerciseModifyModalState();
}

class _ExerciseModifyModalState extends State<ExerciseModifyModal> {

  Widget _createTitle(String text, void onChange(String newVal)) {
    Utilities.validateExerciseName(text);
    return Center(
        child: Padding(
      padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: TextFormField(
        textAlign: TextAlign.center,
        initialValue: text,
        style: Theme.of(context).primaryTextTheme.headline6,
        onFieldSubmitted: onChange,
        validator: (value) => validatedMessage,
        decoration: InputDecoration(hintText: "Exercise Name"),
      ),
    ));
  }

  Widget _createSubtitle(String text) {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(bottom: 8.0),
        child: Text(text, style: Theme.of(context).primaryTextTheme.subtitle2),
      ),
    );
  }

  bool _creatingExercise;
  Exercise _modifiedExercise;
  Exercise _currentExercise;
  bool _modified = false;
  GlobalKey<FormState> _formKey = GlobalKey();

  String validatedMessage;

  @override
  void initState() {
    super.initState();

    _modifiedExercise = widget.exercise.clone();
    _currentExercise = widget.exercise;
    _creatingExercise = widget.exercise.name.isEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            _createTitle(_modifiedExercise.name, (newVal) async {
              _modifiedExercise.name = newVal;
              validatedMessage =
                  await Utilities.validateExerciseName(_modifiedExercise.name);
            }),
            _createSubtitle("Primary Muscle Group"),
            EnumRadio(
                enumValues: ExerciseGroup.values,
                value: _modifiedExercise.primaryGroup,
                onChange: (newVal) {
                  setState(() {
                    _modifiedExercise.primaryGroup = newVal;
                    _modifiedExercise.secondaryGroups.remove(newVal);
                  });
                }),
            Divider(),
            _createSubtitle("Intensity"),
            EnumRadio(
                enumValues: ExerciseIntensity.values,
                value: _modifiedExercise.intensity,
                onChange: (newVal) {
                  setState(() {
                    _modifiedExercise.intensity = newVal;
                  });
                }),
            Divider(),
            _createSubtitle("Secondary Muscle Groups"),
            EnumChecks(
                enumValues: ExerciseGroup.values,
                values: _modifiedExercise.secondaryGroups,
                disabledValue: _modifiedExercise.primaryGroup,
                onChange: (newVal, value) {
                  setState(() {
                    if (newVal) {
                      _modifiedExercise.secondaryGroups.add(value);
                    } else {
                      _modifiedExercise.secondaryGroups.remove(value);
                    }
                  });
                }),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  TextButton(
                      text: "Save",
                      onPress: () async {
                        bool valid = true;
                        validatedMessage = await Utilities.validateExerciseName(
                            _modifiedExercise.name);
                        // Only validate if there is a change to the name.
                        // This prevents 'renaming' an exercise to itself.
                        if (_creatingExercise ||
                            _currentExercise.name != _modifiedExercise.name) {
                          valid = _formKey.currentState.validate();
                        }
                        if (valid) {
                          _creatingExercise = false;
                          _modified = true;
                          AppData.modifyExercise(
                                  _currentExercise, _modifiedExercise)
                              .then((_) {
                            _currentExercise.copyFrom(_modifiedExercise);
                          });
                        }
                      }),
                  TextButton(
                      text: "Finish",
                      onPress: () async {
                        bool finished = true;
                        if (_currentExercise.notEquals(_modifiedExercise)) {
                          finished = await showDialog(
                            context: context,
                            builder: (context) => DiscardChangesConfirmDialog(
                              creating: _creatingExercise,
                              exercise: _currentExercise,
                              modifiedExercise: _modifiedExercise,
                            ),
                          );
                        }
                        if (finished) {
                          Navigator.of(context).pop(_modified);
                        }
                      }),
                ]),
          ],
        ),
      ),
    );
  }
}
