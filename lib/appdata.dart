import 'package:cuoc_circuits_flutter/database.dart';
import 'package:cuoc_circuits_flutter/exercises.dart';

class AppData {
  static double _beepVolume = 1.0;
  static double _voiceVolume = 1.0;
  static Duration defaultWorkoutTime = Duration(minutes: 20);

  static final Duration _maxWorkoutTime = Duration(minutes: 120);
  static final Duration _minWorkoutTime = Duration(minutes: 5);

  static Duration get maxWorkoutTime => _maxWorkoutTime;
  static Duration get minWorkoutTime => _minWorkoutTime;

  static Future<void> initDatabase() async {
    await AppDatabase.initDatabase();
    await loadPreferences();
  }

  static Future<List<Exercise>> getExercises() async {
    List<Exercise> exerciseList = await AppDatabase.getExercises();
    return exerciseList;
  }

  static Future<bool> exerciseNameExists(String name) async {
    bool exists = await AppDatabase.exerciseNameExists(name);
    return exists;
  }

  static Future<bool> addExerciseToDatabase(Exercise toAdd) async {
    bool result = await AppDatabase.addExerciseToDatabase(toAdd);
    return result;
  }

  static Future<bool> modifyExercise(
      Exercise oldExercise, Exercise newExercise) async {
    bool result = await AppDatabase.modifyExercise(oldExercise, newExercise);
    return result;
  }

  static Future<bool> removeExerciseFromDatabase(Exercise toRemove) async {
    AppDatabase.removeExerciseFromDatabase(toRemove);
    return true;
  }

  static double get beepVolume => _beepVolume;
  static double get voiceVolume => _voiceVolume;

  static set beepVolume(double newVolume) =>
      _beepVolume = newVolume.clamp(0.0, 1.0);

  static set voiceVolume(double newVolume) =>
      _voiceVolume = newVolume.clamp(0.0, 1.0);

  static Future<T> getPreference<T>(String name) async {
    return await AppDatabase.getPreference(name);
  }

  static Future<void> setPreference(String name, String value) async {
    await AppDatabase.setPreference(name, value);
  }

  static Future<void> loadPreferences() async {
    AppData.beepVolume = await getPreference("BeepVolume") ?? 1.0;
    AppData.voiceVolume = await getPreference("VoiceVolume") ?? 1.0;
    AppData.defaultWorkoutTime = Duration(
        seconds: await getPreference("DefaultTime") ??
            20 * Duration.secondsPerMinute);
  }
}
