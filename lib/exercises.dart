enum ExerciseGroup {
  CORE,
  CHEST,
  LEGS,
  GLUTES,
  ARMS,
}

enum ExerciseIntensity {
  PASSIVE,
  LOW,
  MODERATE,
  HIGH,
  EXTREME,
}

class Exercise {
  String name;
  ExerciseGroup primaryGroup;
  Set<ExerciseGroup> secondaryGroups;
  ExerciseIntensity intensity;

  Exercise(this.name, this.intensity, this.primaryGroup,
      [this.secondaryGroups = const {}]);

  get hasSecondaryGroups => secondaryGroups.isNotEmpty;

  bool equals(Exercise other) {
    return (this.name.compareTo(other.name) == 0 &&
        this.primaryGroup == other.primaryGroup &&
        this.intensity == other.intensity &&
        this.secondaryGroups.difference(other.secondaryGroups).isEmpty);
  }

  bool notEquals(Exercise other) {
    return !equals(other);
  }

  void copyFrom(Exercise other) {
    this.name = String.fromCharCodes(other.name.runes);
    this.intensity = other.intensity;
    this.primaryGroup = other.primaryGroup;
    this.secondaryGroups = Set<ExerciseGroup>.from(other.secondaryGroups);
  }

  Exercise clone() {
    Exercise cloned = Exercise("", this.intensity, this.primaryGroup);
    cloned.copyFrom(this);
    return cloned;
  }
}

Exercise restExercise = Exercise(
    "REST BETWEEN SETS", ExerciseIntensity.PASSIVE, ExerciseGroup.CORE);

List<Exercise> defaultExercises = [
  Exercise("Press ups", ExerciseIntensity.HIGH, ExerciseGroup.CHEST,
      {ExerciseGroup.ARMS, ExerciseGroup.CORE}),
  Exercise("Squats", ExerciseIntensity.MODERATE, ExerciseGroup.LEGS),
  Exercise("Deadbugs", ExerciseIntensity.PASSIVE, ExerciseGroup.CORE),
  Exercise("Bridge", ExerciseIntensity.LOW, ExerciseGroup.GLUTES,
      {ExerciseGroup.CORE}),
  Exercise("Plank", ExerciseIntensity.HIGH, ExerciseGroup.CORE),
  Exercise("Side Plank", ExerciseIntensity.HIGH, ExerciseGroup.CORE),
  Exercise("Mountain Climbers", ExerciseIntensity.EXTREME, ExerciseGroup.CORE,
      {ExerciseGroup.LEGS}),
  Exercise("Single Leg Raises", ExerciseIntensity.MODERATE, ExerciseGroup.CORE),
  Exercise("Clam", ExerciseIntensity.PASSIVE, ExerciseGroup.GLUTES),
  Exercise("Supermans", ExerciseIntensity.LOW, ExerciseGroup.CORE),
  Exercise("Crucifixes", ExerciseIntensity.HIGH, ExerciseGroup.CORE),
  Exercise("Lunges", ExerciseIntensity.LOW, ExerciseGroup.LEGS),
  Exercise("Bicycle Crunches", ExerciseIntensity.EXTREME, ExerciseGroup.CORE),
  Exercise("Sit ups", ExerciseIntensity.HIGH, ExerciseGroup.CORE),
  Exercise("Fire Hydrants", ExerciseIntensity.PASSIVE, ExerciseGroup.GLUTES),
  Exercise("Single Leg Squats", ExerciseIntensity.MODERATE, ExerciseGroup.LEGS),
  Exercise("Calf Raises", ExerciseIntensity.LOW, ExerciseGroup.LEGS),
  Exercise("Wall sits", ExerciseIntensity.LOW, ExerciseGroup.LEGS),
  Exercise("Spiderman Press ups", ExerciseIntensity.HIGH, ExerciseGroup.CHEST,
      {ExerciseGroup.ARMS, ExerciseGroup.CORE}),
];
