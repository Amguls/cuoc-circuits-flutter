import 'package:cuoc_circuits_flutter/utilities.dart';
import 'package:flutter/material.dart';

import 'package:cuoc_circuits_flutter/appdata.dart';
import 'package:cuoc_circuits_flutter/workout_select.dart';

class WorkoutTimePage extends StatefulWidget {
  @override
  _WorkoutTimePageState createState() => _WorkoutTimePageState();
}

class _WorkoutTimePageState extends State<WorkoutTimePage> {
  Duration _workoutTime = AppData.defaultWorkoutTime;

  void _modifyTime(Duration modifier) {
    setState(() {
      _workoutTime += modifier;
      _workoutTime = (_workoutTime > AppData.minWorkoutTime)
          ? _workoutTime
          : AppData.minWorkoutTime;
      _workoutTime = (_workoutTime < AppData.maxWorkoutTime)
          ? _workoutTime
          : AppData.maxWorkoutTime;
    });
  }

  void resetTime() {
    setState(() {
      _workoutTime = AppData.defaultWorkoutTime;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(
                'Workout Time:',
                style: Theme.of(context).textTheme.headline5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextButton(
                          text: "-10m",
                          onPress: () => _modifyTime(Duration(minutes: -10))),
                      TextButton(
                          text: "-5m",
                          onPress: () => _modifyTime(Duration(minutes: -5))),
                    ],
                  ),
                  Text(
                    '${Utilities.formatTime(_workoutTime)}',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextButton(
                          text: "+10m",
                          onPress: () => _modifyTime(Duration(minutes: 10))),
                      TextButton(
                          text: "+5m",
                          onPress: () => _modifyTime(Duration(minutes: 5))),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  TextButton(text: "Reset", onPress: resetTime),
                  TextButton(
                      text: "Create",
                      onPress: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => WorkoutSelectPage(
                                workoutDuration: _workoutTime)));
                      }),
                ],
              ),
            ]),
      ),
    );
  }
}
