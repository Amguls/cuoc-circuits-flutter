import 'package:flutter/material.dart';

const cambridgeHexValue = 0xffa3c1ad;
const cuocHexValue = 0xffceedf5;
const darkCuocHexValue = 0xff31120a;

const Map<int, Color> cambridgeColor = {
  50: Color.fromRGBO(163, 193, 173, .1),
  100: Color.fromRGBO(163, 193, 173, .2),
  200: Color.fromRGBO(163, 193, 173, .3),
  300: Color.fromRGBO(163, 193, 173, .4),
  400: Color.fromRGBO(163, 193, 173, .5),
  500: Color.fromRGBO(163, 193, 173, .6),
  600: Color.fromRGBO(163, 193, 173, .7),
  700: Color.fromRGBO(163, 193, 173, .8),
  800: Color.fromRGBO(163, 193, 173, .9),
  900: Color.fromRGBO(163, 193, 173, 1),
};

const Map<int, Color> cuocColor = {
  50: Color.fromRGBO(206, 237, 245, .1),
  100: Color.fromRGBO(206, 237, 245, .2),
  200: Color.fromRGBO(206, 237, 245, .3),
  300: Color.fromRGBO(206, 237, 245, .4),
  400: Color.fromRGBO(206, 237, 245, .5),
  500: Color.fromRGBO(206, 237, 245, .6),
  600: Color.fromRGBO(206, 237, 245, .7),
  700: Color.fromRGBO(206, 237, 245, .8),
  800: Color.fromRGBO(206, 237, 245, .9),
  900: Color.fromRGBO(206, 237, 245, 1),
};

const Map<int, Color> darkCuocColor = {
  50: Color.fromRGBO(49, 18, 10, .1),
  100: Color.fromRGBO(49, 18, 10, .2),
  200: Color.fromRGBO(49, 18, 10, .3),
  300: Color.fromRGBO(49, 18, 10, .4),
  400: Color.fromRGBO(49, 18, 10, .5),
  500: Color.fromRGBO(49, 18, 10, .6),
  600: Color.fromRGBO(49, 18, 10, .7),
  700: Color.fromRGBO(49, 18, 10, .8),
  800: Color.fromRGBO(49, 18, 10, .9),
  900: Color.fromRGBO(49, 18, 10, 1),
};

const cambridgeMaterial = MaterialColor(cambridgeHexValue, cambridgeColor);
const cuocMaterial = MaterialColor(cuocHexValue, cuocColor);
const darkCuocMaterial = MaterialColor(darkCuocHexValue, darkCuocColor);