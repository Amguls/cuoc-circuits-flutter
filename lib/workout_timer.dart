import 'package:cuoc_circuits_flutter/colours.dart';
import 'package:flutter/material.dart';
import 'dart:async';

abstract class TimedCallback {
  TimedCallback(this._callback);

  final void Function() _callback;

  bool _triggered = false;

  void trigger() {
    _callback();
    _triggered = true;
  }

  bool _shouldTrigger(double elapsedTime, double totalTime);
  bool shouldTrigger(double elapsedTime, double totalTime) {
    if (_triggered) return false;
    return _shouldTrigger(elapsedTime, totalTime);
  }
}

class TimedCallbackAbsolute extends TimedCallback {
  TimedCallbackAbsolute(this.time, void Function() callback) : super(callback);

  final Duration time;

  @override
  bool _shouldTrigger(double elapsedTime, double totalTime) {
    return (time.inMilliseconds / 1000 >= totalTime - elapsedTime);
  }
}

class TimedCallbackProgress extends TimedCallback {
  TimedCallbackProgress(this.time, void Function() callback) : super(callback);

  final double time;

  @override
  bool _shouldTrigger(double elapsedTime, double totalTime) {
    return (time >= (totalTime - elapsedTime) / totalTime);
  }
}

class WorkoutTimer extends StatefulWidget {
  WorkoutTimer({Key key, @required this.time, this.onTick, this.onFinished, this.callbacks})
      : super(key: key);

  final double time;
  final List<TimedCallback> callbacks;
  final void Function() onFinished;
  final void Function(double) onTick;

  @override
  WorkoutTimerState createState() => WorkoutTimerState();
}

class WorkoutTimerState extends State<WorkoutTimer>  {
  bool _paused = true;
  get isPaused => _paused;

  // Progress to final time expressed as a value [0,1].
  double _timeProgress = 0.0;
  // Time for which the timer has been active.
  double _elapsedTime = 0.0;
  // The time at which the timer was last paused.
  // Used since we have to create a new timer after each pause.
  double _savedTime = 0.0;

  Timer _activeTimer;

  final Duration _stepDuration = Duration(milliseconds: 10);

  void _initialiseTimer() {
    setState(() {
      _createTimer();
      _elapsedTime = 0.0;
      _timeProgress = 0.0;
      _savedTime = 0.0;
    });
  }

  void _updateProgress(Timer timer) {
    _elapsedTime =
        _savedTime + timer.tick * _stepDuration.inMilliseconds / 1000.0;
    _timeProgress = _elapsedTime / widget.time;
  }

  void _processCallbacks() {
    for (TimedCallback cb in widget.callbacks) {
      if (cb.shouldTrigger(_elapsedTime, widget.time)) {
        cb.trigger();
      }
    }
    widget.onTick(_elapsedTime);
  }

  void _timerCallback(Timer timer) {
    if (!mounted) return;
    setState(() {
      _updateProgress(timer);
      _processCallbacks();
      if (_timeProgress >= 1.0) {
        widget.onFinished();
        timer?.cancel();
      }
    });
  }

  void start() async {
    _initialiseTimer();
    _paused = false;
  }

  @override
  void dispose() {
    _activeTimer?.cancel();
    super.dispose();
  }

  void pause() {
    if (_paused) return;

    _activeTimer?.cancel();
    _savedTime = _elapsedTime;

    setState(() {
      _paused = true;
    });
  }

  void _createTimer() {
    _activeTimer?.cancel();
    _activeTimer = new Timer.periodic(_stepDuration, _timerCallback);
  }

  void reset() {
    _activeTimer?.cancel();

    setState(() {
      _savedTime = 0.0;
      _elapsedTime = 0.0;
      _timeProgress = 0.0;
    });

    if (!_paused) {
      _createTimer();
    }
  }

  void resume() {
    if (!_paused) return;

    _createTimer();

    setState(() {
      _paused = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: AspectRatio(
        aspectRatio: 1.0,
        child: CircularProgressIndicator(
          value: _timeProgress,
          backgroundColor: Theme.of(context).backgroundColor,
          strokeWidth: 20.0,
          valueColor: AlwaysStoppedAnimation(darkCuocColor[900]),
        ),
      ),
    );
  }
}
