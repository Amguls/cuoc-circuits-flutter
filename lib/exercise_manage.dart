import 'package:cuoc_circuits_flutter/exercise_modify.dart';
import 'package:cuoc_circuits_flutter/exercises.dart';
import 'package:cuoc_circuits_flutter/utilities.dart';
import 'package:flutter/material.dart';

import 'package:cuoc_circuits_flutter/appdata.dart';

class DeleteExerciseConfirmDialog extends StatelessWidget {
  DeleteExerciseConfirmDialog(
      {@required this.exercise, @required this.onDelete});

  final Exercise exercise;
  final void Function(bool) onDelete;

  @override
  Widget build(context) {
    return AlertDialog(
      title: Text("Confirm"),
      content: Text("Delete exercise \"${exercise.name}\"?"),
      actions: <Widget>[
        TextButton(
          text: "Delete",
          onPress: () {
            AppData.removeExerciseFromDatabase(exercise).then(onDelete);
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          text: "Cancel",
          onPress: Navigator.of(context).pop,
        ),
      ],
    );
  }
}

class ExerciseManagePage extends StatefulWidget {
  @override
  _ExerciseManagePageState createState() => _ExerciseManagePageState();
}

class _ExerciseManagePageState extends State<ExerciseManagePage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  List<Exercise> exercises = [];
  Future<void> completedSelection;

  Future<void> retrieveExercises() async {
    List<Exercise> tmp = await AppData.getExercises();
    setState(() {
      exercises = tmp;
    });
  }

  void _buildModifyExercise(BuildContext context, Exercise exercise) {
    showModalBottomSheet(
            context: context,
            builder: (context) => ExerciseModifyModal(exercise),
            isDismissible: false)
        .then((modified) {
      if (modified) {
        setState(() {
          completedSelection = retrieveExercises();
        });
      }
    });
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.pop(context, false),
      ),
      title: Text("Exercises"),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () {
            _buildModifyExercise(context,
                Exercise("", ExerciseIntensity.PASSIVE, ExerciseGroup.CORE));
          },
        )
      ],
    );
  }

  void showSnackBar(BuildContext context, String text) =>
      Utilities.showSnackBar(context, text);

  void showDeleteConfirmDialog(BuildContext context, Exercise exercise) {
    showDialog(
      context: context,
      builder: DeleteExerciseConfirmDialog(
        exercise: exercise,
        onDelete: (deleted) {
          if (deleted) {
            setState(() {
              exercises.remove(exercise);
            });
            showSnackBar(context, "Deleted exercise");
          } else {
            showSnackBar(context, "Failed to delete exercise - not found");
          }
        },
      ).build,
    );
  }

  Widget _buildExerciseList(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: FutureBuilder<void>(
                  future: completedSelection,
                  builder:
                      (BuildContext context, AsyncSnapshot<void> snapshot) {
                    return ExerciseList(
                        values: exercises,
                        onTap: (exercise) =>
                            _buildModifyExercise(context, exercise),
                        builder: (context, exercise) {
                          return ListTile(
                            trailing: IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () =>
                                    showDeleteConfirmDialog(context, exercise)),
                            title: Text(
                              exercise.name,
                            ),
                          );
                        },
                        snapshot: snapshot);
                  }),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    completedSelection = retrieveExercises();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      body: _buildExerciseList(context),
    );
  }
}
