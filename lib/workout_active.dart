import 'package:cuoc_circuits_flutter/colours.dart';
import 'package:cuoc_circuits_flutter/exercises.dart';
import 'package:cuoc_circuits_flutter/utilities.dart';
import 'package:cuoc_circuits_flutter/workout_timer.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter_tts/flutter_tts.dart';

import 'package:cuoc_circuits_flutter/appdata.dart';
import 'package:cuoc_circuits_flutter/workout_plan.dart';

class WorkoutActivePage extends StatefulWidget {
  WorkoutActivePage({Key key, this.workoutPlan, this.exercises})
      : super(key: key);

  final WorkoutPlan workoutPlan;
  final List<Exercise> exercises;

  @override
  _WorkoutActivePageState createState() => _WorkoutActivePageState();
}

class _WorkoutActivePageState extends State<WorkoutActivePage>
    with SingleTickerProviderStateMixin {
  AudioCache _audioPlayer = new AudioCache(fixedPlayer: new AudioPlayer());
  FlutterTts _tts = new FlutterTts();

  GlobalKey<WorkoutTimerState> _timerKey = new GlobalKey<WorkoutTimerState>();
  List<TimedCallback> _timerCallbacks = [];

  String _ext = (Platform.isAndroid) ? '.ogg' : '.mp3';

  get paused => _timerKey.currentState.isPaused;
  set paused(bool newVal) {
    if (newVal) {
      _animationController.reverse();
      _timerKey.currentState.pause();
    } else {
      _animationController.forward();
      _timerKey.currentState.resume();
    }
  }

  double elapsedTime = 0.0;

  AnimationController _animationController;

  ActiveState activeState;

  void _nextExercise() {
    assert(widget.workoutPlan.isNotComplete);
    if (widget.workoutPlan.remaining == 0) {
      _speak('Final exercise: ' +
          widget.workoutPlan.getCurrentExercise().name +
          '.');
    } else {
      _speak('Next exercise: ' +
          widget.workoutPlan.getCurrentExercise().name +
          '.');
    }
  }

  void _exerciseTenSeconds() {
    assert(widget.workoutPlan.isNotComplete);
    if (widget.workoutPlan.remaining == 0) {
      _speak('In ten seconds your workout is finished.');
    } else if (widget.workoutPlan.next.type == ActiveState.REST) {
      _speak('In ten seconds rest between sets.');
    } else {
      _speak('Ten seconds remaining.');
    }
  }

  void _exerciseHalfway() {
    _tts.speak("Halfway");
  }

  List<TimedCallback> _beepCallbacks() {
    return [
      TimedCallbackAbsolute(Duration(seconds: 4), _playShortBeep),
      TimedCallbackAbsolute(Duration(seconds: 3), _playShortBeep),
      TimedCallbackAbsolute(Duration(seconds: 2), _playShortBeep),
      TimedCallbackAbsolute(Duration(seconds: 1), _playShortBeep),
      TimedCallbackAbsolute(Duration(seconds: 0), _playLongBeep)
    ];
  }

  List<TimedCallback> _transitionCallbacks() {
    return <TimedCallback>[
      TimedCallbackAbsolute(Duration(seconds: 9), _nextExercise)
    ].followedBy(_beepCallbacks()).toList();
  }

  List<TimedCallback> _exerciseCallbacks() {
    return <TimedCallback>[
      TimedCallbackAbsolute(Duration(seconds: 10), _exerciseTenSeconds),
      TimedCallbackProgress(0.501, _exerciseHalfway)
    ].followedBy(_beepCallbacks()).toList();
  }

  List<TimedCallback> _restCallbacks() {
    return <TimedCallback>[
      TimedCallbackAbsolute(Duration(seconds: 10), _nextExercise)
    ].followedBy(_beepCallbacks()).toList();
  }

  Map<ActiveState, List<TimedCallback> Function()> _genCallbacks;

  void setTimerCallbacksForState(ActiveState state) {
    assert(_genCallbacks.containsKey(state));
    List<TimedCallback> callbacks = _genCallbacks[state]();
    setState(() {
      _timerCallbacks = callbacks;
    });
  }

  void clearCallbacks() {
    setState(() {
      _timerCallbacks = [];
    });
  }

  void moveNext() {
    if (widget.workoutPlan.remaining == 0 &&
        widget.workoutPlan.current.type == ActiveState.EXERCISE) {
      activeState = ActiveState.COMPLETE;
      // TODO: Play victory jingle.
      Navigator.of(context).popUntil((Route route) => route.isFirst);
      return;
    }
    bool move = widget.workoutPlan.moveNext();

    assert(move);

    setState(() {
      activeState = widget.workoutPlan.current.type;
      elapsedTime = 0.0;
    });
    clearCallbacks();
    _timerKey.currentState.reset();

    if (_genCallbacks.containsKey(activeState)) {
      setTimerCallbacksForState(activeState);
    }
  }

  void _play(String name) {
    _audioPlayer.play(name + _ext, volume: AppData.beepVolume);
  }

  void _playShortBeep() => _play('beep_get_ready');
  void _playLongBeep() => _play('beep_start');

  void _speak(String toSpeak) {
    _tts.setVolume(AppData.voiceVolume);
    _tts.speak(toSpeak);
  }

  void beginWorkout() async {
    setState(() {
      activeState = widget.workoutPlan.current.type;
    });
    setTimerCallbacksForState(activeState);
    _timerKey.currentState.start();
    paused = false;
  }

  @override
  void initState() {
    super.initState();

    _genCallbacks = {
      ActiveState.TRANSITION: _transitionCallbacks,
      ActiveState.EXERCISE: _exerciseCallbacks,
      ActiveState.REST: _restCallbacks,
    };

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 100));

    activeState = ActiveState.NOT_STARTED;
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Stack(
              children: <Widget>[
                Center(
                  child: WorkoutTimer(
                    key: _timerKey,
                    time: widget.workoutPlan.current.duration.inSeconds
                        .toDouble(),
                    onTick: (time) {
                      setState(() {
                        elapsedTime = time;
                      });
                    },
                    callbacks: _timerCallbacks,
                    onFinished: moveNext,
                  ),
                ),
                Center(
                  child: (activeState == ActiveState.NOT_STARTED)
                      ? TextButton(text: 'Begin', onPress: beginWorkout)
                      : Text(
                          (widget.workoutPlan.current.duration.inSeconds -
                                      elapsedTime)
                                  .toStringAsFixed(1) +
                              's',
                          style: Theme.of(context).primaryTextTheme.headline5),
                ),
              ],
            ),
          ),
          Expanded(
            child: Center(
              child: Card(
                color: Theme.of(context).accentColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    IconButton(
                        icon: AnimatedIcon(
                            icon: AnimatedIcons.play_pause,
                            progress: _animationController,
                            color: cambridgeColor[900]),
                        onPressed: () {
                          if (activeState == ActiveState.NOT_STARTED) {
                            beginWorkout();
                          } else {
                            paused = !paused;
                          }
                        }),
                    Center(
                        child: Text(
                      (activeState == ActiveState.TRANSITION
                          ? 'Next: ${widget.workoutPlan.getCurrentExercise().name}'
                          : '${widget.workoutPlan.getCurrentExercise().name}'),
                      style: Theme.of(context).primaryTextTheme.headline6,
                      maxLines: 2,
                    )),
                    IconButton(
                        icon: Icon(Icons.skip_next, color: cambridgeColor[900]),
                        onPressed: moveNext),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Center(
              child: ListView.builder(
                itemCount: widget.workoutPlan.remaining,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext c, int index) => Card(
                    color: index.isOdd
                        ? Theme.of(context).accentColor
                        : Theme.of(context).backgroundColor,
                    child: ListTile(
                        title: Text(widget.workoutPlan
                            .getExerciseOffset(index + 1)
                            .name),
                        dense: true)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
