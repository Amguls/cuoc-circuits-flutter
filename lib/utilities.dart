import 'package:cuoc_circuits_flutter/appdata.dart';
import 'package:cuoc_circuits_flutter/colours.dart';
import 'package:flutter/material.dart';

class Utilities {
  static String formatTime(Duration time) {
    int minutes = time.inMinutes;
    int seconds = time.inSeconds.remainder(Duration.secondsPerMinute);
    StringBuffer sb = new StringBuffer();
    sb.write('$minutes:');
    if (seconds < 10) {
      sb.write('0');
    }
    sb.write(seconds);
    return sb.toString();
  }

  static String getEnumName<T>(T enumValue) {
    String name = enumValue.toString().split('.').last;
    return name[0].toUpperCase() + name.substring(1).toLowerCase();
  }

  static void showSnackBar(BuildContext context, String text,
      [Duration duration = const Duration(seconds: 2)]) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        duration: duration,
      ),
    );
  }

  static Future<String> validateExerciseName(String value) async {
    if (value.isEmpty) {
      return "Please enter a name";
    }
    if (await AppData.exerciseNameExists(value)) {
      return "Name already exists";
    }
    return null;
  }
}

class TextButton extends StatelessWidget {
  TextButton({this.text, this.onPress});

  final String text;
  final Function() onPress;

  @override
  Widget build(context) {
    return FlatButton(
      onPressed: onPress,
      child: Text(
        text,
        style: Theme.of(context).primaryTextTheme.button,
      ),
      color: Theme.of(context).primaryColor,
    );
  }
}

class BaseExerciseList<T> extends StatelessWidget {
  BaseExerciseList({@required this.values, @required this.builder, this.onTap})
      : assert(values != null),
        assert(builder != null);

  final List<T> values;
  final Function(T) onTap;
  final Widget Function(BuildContext, T) builder;

  @override
  Widget build(context) {
    return Scrollbar(
      child: ListView(
        children: List<Widget>.from(values
            .asMap()
            .map((index, value) => MapEntry(
                  index,
                  GestureDetector(
                    onTap: () => onTap(value),
                    child: Align(
                      alignment: Alignment.center,
                      child: Card(
                        color: index.isOdd
                            ? Theme.of(context).accentColor
                            : Theme.of(context).backgroundColor,
                        child: builder(context, value),
                      ),
                    ),
                  ),
                ))
            .values),
      ),
    );
  }
}

class ExerciseList<T> extends StatelessWidget {
  ExerciseList(
      {@required this.values,
      @required this.builder,
      @required this.snapshot,
      this.onTap})
      : assert(values != null),
        assert(builder != null),
        assert(snapshot != null);

  final List<T> values;
  final Function(T) onTap;
  final Widget Function(BuildContext, T) builder;
  final AsyncSnapshot<void> snapshot;

  @override
  Widget build(context) {
    switch (snapshot.connectionState) {
      case ConnectionState.none:
        assert(false);
        return null;
      case ConnectionState.active:
      case ConnectionState.waiting:
        return Center(
          child: CircularProgressIndicator(
            backgroundColor: cambridgeColor[900],
            valueColor: AlwaysStoppedAnimation(cuocColor[900]),
          ),
        );
      case ConnectionState.done:
        if (snapshot.hasError)
          return Center(child: Text('Error: ${snapshot.error}'));
        return BaseExerciseList(values: values, onTap: onTap, builder: builder);
    }
    assert(false);
    // Unreachable.
    return null;
  }
}
