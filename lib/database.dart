import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'package:cuoc_circuits_flutter/exercises.dart';

class AppDatabase {
  static final dbName = "Exercises.db";
  static Database db;

  static Future<void> _onOpenDB() async {
    await db.execute("""
      CREATE TABLE IF NOT EXISTS Exercises (id INTEGER PRIMARY KEY,
                                            name TEXT,
                                            intensity INTEGER,
                                            primaryGroup INTEGER)""");

    await db.execute("""
      CREATE TABLE IF NOT EXISTS SecondaryGroups (id INTEGER,
                                                  groupValue INTEGER,
                                                  FOREIGN KEY(id) REFERENCES Exercises(id))""");

    await db.execute("""
      CREATE TABLE IF NOT EXISTS Preferences (name TEXT PRIMARY KEY,
                                              value TEXT)""");
  }

  static Future<void> _openDatabase(String path, bool exists) async {
    db = await openDatabase(path);
    await _onOpenDB();
    // Populate database tables with default exercise info if they did not previously exist.
    if (!exists) {
      for (var exercise in defaultExercises) {
        addExerciseToDatabase(exercise);
      }
    }
  }

  static Future<void> initDatabase() async {
    var databasesPath = await getDatabasesPath();

    String path = join(databasesPath, dbName);

    bool exists = await databaseExists(path);

    if (!exists) {
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (_) {}
    }

    await _openDatabase(path, exists);
  }

  static Future<Exercise> _databaseRowToExercise(
      Map<String, dynamic> row) async {
    int id = row['id'];
    String name = row['name'];
    ExerciseIntensity intensity = ExerciseIntensity.values[row['intensity']];
    ExerciseGroup primaryGroup = ExerciseGroup.values[row['primaryGroup']];

    List<Map<String, dynamic>> secondGroups =
        await db.query("SecondaryGroups", where: "id=?", whereArgs: [id]);

    Set<ExerciseGroup> secondaryGroups = Set<ExerciseGroup>();

    for (var secondary in secondGroups) {
      secondaryGroups.add(ExerciseGroup.values[secondary['groupValue']]);
    }

    return Exercise(name, intensity, primaryGroup, secondaryGroups);
  }

  static Map<String, dynamic> _exerciseToDatabaseRow(Exercise exercise) {
    Map<String, dynamic> values = Map<String, dynamic>();
    values['name'] = exercise.name;
    values['intensity'] = exercise.intensity.index;
    values['primaryGroup'] = exercise.primaryGroup.index;

    return values;
  }

  static Future<List<Exercise>> getExercises() async {
    List<Map<String, dynamic>> exerciseRows = await db.query("Exercises");
    List<Exercise> exerciseList = List<Exercise>();
    for (var row in exerciseRows) {
      exerciseList.add(await _databaseRowToExercise(row));
    }
    return exerciseList;
  }

  static Future<bool> exerciseNameExists(String name) async {
    List<Map<String, dynamic>> results =
        await db.query("Exercises", where: "name=?", whereArgs: [name]);

    return results.isNotEmpty;
  }

  static Future<bool> addExerciseToDatabase(Exercise toAdd) async {
    Map<String, dynamic> values = _exerciseToDatabaseRow(toAdd);

    if (await exerciseNameExists(toAdd.name)) {
      return false;
    }

    var batch = db.batch();
    int newId = await db.insert("Exercises", values);

    if (toAdd.hasSecondaryGroups) {
      for (ExerciseGroup group in toAdd.secondaryGroups) {
        batch.insert(
            "SecondaryGroups", {'id': newId, 'groupValue': group.index});
      }
    }

    await batch.commit();
    return true;
  }

  static Future<bool> modifyExercise(
      Exercise oldExercise, Exercise newExercise) async {
    List<String> name = [oldExercise.name];
    List<Map<String, dynamic>> results =
        await db.query("Exercises", where: "name=?", whereArgs: name);

    // No such exercise exists.
    if (results.isEmpty) {
      return addExerciseToDatabase(newExercise);
    }

    assert(results.length == 1);
    int id = results.first['id'];

    var values = _exerciseToDatabaseRow(newExercise);

    Batch batch = db.batch();
    batch.update("Exercises", values, where: "id=?", whereArgs: [id]);
    batch.delete("SecondaryGroups", where: "id=?", whereArgs: [id]);
    for (ExerciseGroup group in newExercise.secondaryGroups) {
      batch.insert("SecondaryGroups", {'id': id, 'groupValue': group.index});
    }
    await batch.commit();
    return true;
  }

  static Future<bool> removeExerciseFromDatabase(Exercise toRemove) async {
    List<Map<String, dynamic>> results = await db
        .query("Exercises", where: "name=?", whereArgs: [toRemove.name]);

    // No such exercise exists.
    if (results.isEmpty) return false;

    assert(results.length == 1);
    int id = results.first['id'];

    Batch batch = db.batch();
    batch.delete("Exercises", where: "name=?", whereArgs: [toRemove.name]);

    if (toRemove.hasSecondaryGroups) {
      batch.delete("SecondaryGroups", where: "id=?", whereArgs: [id]);
    }

    await batch.commit();
    return true;
  }

  static Future<T> getPreference<T>(String name) async {
    var result =
        await db.query("Preferences", where: "name=?", whereArgs: [name]);

    if (result.isEmpty) return null;

    String value = result.first['value'];
    if (value == null) {
      return null;
    }

    switch (T) {
      case int:
        return int.parse(value) as T;
      case double:
        return double.parse(value) as T;
      case String:
        return value as T;
    }

    return null;
  }

  static Future<void> setPreference(String name, String value) async {
    var result =
        await db.query("Preferences", where: "name=?", whereArgs: [name]);

    if (result.isEmpty) {
      await db.insert("Preferences", {"name": name, "value": value});
    } else {
      await db.update("Preferences", {"value": value},
          where: "name=?", whereArgs: [name]);
    }
  }
}