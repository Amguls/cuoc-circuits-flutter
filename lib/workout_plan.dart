import 'package:cuoc_circuits_flutter/appdata.dart';
import 'dart:math';

import 'package:cuoc_circuits_flutter/exercises.dart';

final _random = Random();

enum ActiveState {
  NOT_STARTED,
  EXERCISE,
  TRANSITION,
  REST,
  COMPLETE,
}

class Activity {
  ActiveState type;
  Duration duration;

  Activity(this.type, this.duration);
}

class WorkoutPlan extends Iterator {
  int _currentIndex = 0;
  int _exerciseIndex = 0;
  List<Activity> activities = List<Activity>();
  List<Exercise> exercises;
  String name;

  Exercise getCurrentExercise() => getExerciseOffset(0);
  Exercise getNextExercise() => getExerciseOffset(1);
  Exercise getExerciseOffset(int index) {
    int numRests = getNumRestsInNext(index);
    if (getOffset(index).type == ActiveState.REST) {
      return restExercise;
    } else {
      return exercises[(_exerciseIndex + index - numRests) % exercises.length];
    }
  }

  void add(activity) {
    activities.add(activity);
  }

  void reset() {
    _currentIndex = 0;
    _exerciseIndex = 0;
  }

  get totalDuration => activities.fold(
      Duration.zero, (duration, activity) => duration + activity.duration);

  Activity get first => activities.first;
  int get remaining {
    return activities
            .sublist(_currentIndex)
            .where((a) => a.type != ActiveState.TRANSITION)
            .length -
        1;
  }

  @override
  Activity get current => activities[_currentIndex];

  Activity get next => activities[_currentIndex + 1];

  Activity getOffset(int index) => activities
      .sublist(_currentIndex)
      .where((a) => a.type != ActiveState.TRANSITION)
      .elementAt(index);

  int getNumRestsInNext(int index) => activities
      .sublist(_currentIndex)
      .where((a) => a.type != ActiveState.TRANSITION)
      .take(index)
      .where((activity) => activity.type == ActiveState.REST)
      .length;

  @override
  bool moveNext() {
    if (current.type == ActiveState.EXERCISE) _exerciseIndex += 1;
    _currentIndex += 1;
    return isNotComplete;
  }

  bool get isComplete => (_currentIndex >= activities.length);
  bool get isNotComplete => !isComplete;

  int get numExercisesInSet {
    int firstIndex =
        activities.indexWhere((activity) => activity.type == ActiveState.REST);
    List oneSet;
    if (firstIndex >= 0) {
      oneSet = activities.sublist(
          0,
          activities
              .indexWhere((activity) => activity.type == ActiveState.REST));
    } else {
      oneSet = activities;
    }
    return oneSet
        .where((activity) => activity.type == ActiveState.EXERCISE)
        .length;
  }

  int get numSets {
    return activities
            .where((activity) => activity.type == ActiveState.REST)
            .toList()
            .length +
        1;
  }

  Future<List<Exercise>> selectExercises(Duration time,
      [ExerciseIntensity minIntensity = ExerciseIntensity.PASSIVE,
      ExerciseIntensity maxIntensity = ExerciseIntensity.EXTREME]) async {
    List<Exercise> allExercises = await AppData.getExercises();

    int numExercises = numExercisesInSet;
    List<Exercise> selectable = List<Exercise>.from(allExercises.where(
        (exercise) =>
            exercise.intensity.index >= minIntensity.index &&
            exercise.intensity.index <= maxIntensity.index));

    List<Exercise> selected = [];

    Exercise lastExercise = selectable[_random.nextInt(selectable.length)];
    selectable.remove(lastExercise);
    selected.add(lastExercise);

    while (selected.length < numExercises) {
      ExerciseGroup lastGroup = lastExercise.primaryGroup;
      ExerciseIntensity lastIntensity = lastExercise.intensity;
      List<Exercise> possible = List<Exercise>.from(selectable.where(
          (exercise) =>
              (lastIntensity.index < ExerciseIntensity.HIGH.index ||
                  exercise.intensity.index < ExerciseIntensity.HIGH.index) &&
              (lastIntensity.index > ExerciseIntensity.LOW.index ||
                  exercise.intensity.index > ExerciseIntensity.LOW.index) &&
              (exercise.primaryGroup != lastGroup ||
                  (exercise.intensity.index <
                          ExerciseIntensity.MODERATE.index &&
                      lastIntensity.index < ExerciseIntensity.EXTREME.index))));
      if (possible.isEmpty) {
        // ...
      } else {
        // TODO: Remember previous sessions and weight exercise choice.
        lastExercise = possible[_random.nextInt(possible.length)];
      }
      selectable.remove(lastExercise);
      selected.add(lastExercise);
    }
    exercises = selected;
    return selected;
  }

  static WorkoutPlan pyramidWorkout(Duration maxDuration) {
    WorkoutPlan pyramid = WorkoutPlan();
    pyramid.name = "Pyramid Workout";

    Duration transitionDuration = Duration(seconds: 10);
    Duration restDuration = Duration(seconds: 120);
    int numSets = (maxDuration.inMinutes ~/ 15) + 1;
    Duration maxSetDuration = Duration(
        seconds: (maxDuration.inSeconds -
                ((numSets - 1) * restDuration.inSeconds)) ~/
            numSets);

    int exercisesInSet;
    if (maxSetDuration <= Duration(minutes: 5)) {
      exercisesInSet = 5;
    } else if (maxDuration <= Duration(minutes: 10)) {
      exercisesInSet = 7;
    } else {
      exercisesInSet = 9;
    }
    Duration baseDuration = Duration(seconds: 30);
    Duration step = Duration(seconds: 10);

    pyramid.add(Activity(ActiveState.TRANSITION, transitionDuration));
    for (int j = 0; j < numSets - 1; ++j) {
      Duration curDuration = baseDuration;
      for (int i = 0; i < exercisesInSet - 1; ++i) {
        pyramid.add(Activity(ActiveState.EXERCISE, curDuration));
        pyramid.add(Activity(ActiveState.TRANSITION, transitionDuration));
        if (i < (exercisesInSet - 1) / 2) {
          curDuration += step;
        } else {
          curDuration -= step;
        }
      }
      pyramid.add(Activity(ActiveState.EXERCISE, curDuration));
      pyramid.add(Activity(ActiveState.REST, restDuration));
    }
    Duration curDuration = baseDuration;
    for (int i = 0; i < exercisesInSet - 1; ++i) {
      pyramid.add(Activity(ActiveState.EXERCISE, curDuration));
      pyramid.add(Activity(ActiveState.TRANSITION, transitionDuration));
      if (i < (exercisesInSet - 1) / 2) {
        curDuration += step;
      } else {
        curDuration -= step;
      }
    }
    pyramid.add(Activity(ActiveState.EXERCISE, curDuration));
    return pyramid;
  }

  static WorkoutPlan standardWorkout(Duration maxDuration) {
    WorkoutPlan standard = WorkoutPlan();
    standard.name = "Standard Workout";

    Duration exerciseDuration = Duration(seconds: 50);
    Duration transitionDuration = Duration(seconds: 10);
    Duration restDuration = Duration(seconds: 120);
    int numSets = (maxDuration.inMinutes ~/ 15) + 1;
    Duration maxSetDuration = Duration(
        seconds: (maxDuration.inSeconds -
                ((numSets - 1) * restDuration.inSeconds)) ~/
            numSets);
    int exercisesInSet =
        (maxSetDuration.inSeconds + transitionDuration.inSeconds) ~/
            (transitionDuration.inSeconds + exerciseDuration.inSeconds);

    standard.add(Activity(ActiveState.TRANSITION, transitionDuration));
    for (int j = 0; j < numSets - 1; ++j) {
      for (int i = 0; i < exercisesInSet - 1; ++i) {
        standard.add(Activity(ActiveState.EXERCISE, exerciseDuration));
        standard.add(Activity(ActiveState.TRANSITION, transitionDuration));
      }
      standard.add(Activity(ActiveState.EXERCISE, exerciseDuration));
      standard.add(Activity(ActiveState.REST, restDuration));
    }
    for (int i = 0; i < exercisesInSet - 1; ++i) {
      standard.add(Activity(ActiveState.EXERCISE, exerciseDuration));
      standard.add(Activity(ActiveState.TRANSITION, transitionDuration));
    }
    standard.add(Activity(ActiveState.EXERCISE, exerciseDuration));
    return standard;
  }
}
