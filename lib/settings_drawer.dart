import 'package:cuoc_circuits_flutter/colours.dart';
import 'package:cuoc_circuits_flutter/exercise_manage.dart';
import 'package:flutter/material.dart';

import 'package:cuoc_circuits_flutter/appdata.dart';

class SettingsDrawer extends StatefulWidget {
  @override
  _SettingsDrawerState createState() => _SettingsDrawerState();
}

class VolumeSetting extends StatelessWidget {
  VolumeSetting({this.name, this.volume, this.onChanged});

  final String name;
  final double volume;
  final Function(double) onChanged;

  Icon _getVolumeIcon(double volume) {
    var iconData;
    if (volume == 0) {
      iconData = Icons.volume_off;
    } else if (volume <= 0.25) {
      iconData = Icons.volume_mute;
    } else if (volume <= 0.75) {
      iconData = Icons.volume_down;
    } else {
      iconData = Icons.volume_up;
    }
    return Icon(
      iconData,
      color: cambridgeColor[900],
    );
  }

  @override
  Widget build(context) {
    return ListTile(
        leading: _getVolumeIcon(volume),
        trailing: Text(name),
        title: Slider(
          label: volume.toStringAsFixed(1),
          value: volume,
          divisions: 10,
          onChanged: (newVolume) async {
            onChanged(newVolume);
            await AppData.setPreference(name + "Volume", newVolume.toString());
          },
        ));
  }
}

class _SettingsDrawerState extends State<SettingsDrawer> {
  String _validate(String value) {
    int number;
    try {
      number = int.parse(value);
    } on FormatException {
      return "Not a number";
    }
    var duration = Duration(minutes: number);
    if (duration > AppData.maxWorkoutTime) {
      return "Too large";
    }
    if (duration < AppData.minWorkoutTime) {
      return "Too low";
    }
    if (number.remainder(5) != 0) {
      return "Multiples of 5 only";
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor,
            ),
            child: Center(
              child: Text("Settings",
                  style: Theme.of(context).primaryTextTheme.headline6),
            ),
          ),
        ].followedBy(
          ListTile.divideTiles(
            context: context,
            tiles: <Widget>[
              ListTile(
                  leading: Icon(
                    Icons.timer,
                    color: cambridgeColor[900],
                  ),
                  title: TextFormField(
                    keyboardType: TextInputType.numberWithOptions(),
                    initialValue:
                        (AppData.defaultWorkoutTime.inMinutes).toString(),
                    decoration: InputDecoration(
                      labelText: "Default Workout Time",
                      suffixText: " minutes",
                    ),
                    onFieldSubmitted: (String value) async {
                      if (_validate(value) != null) {
                        return;
                      }
                      // Save new value.
                      var duration = Duration(minutes: int.parse(value));
                      setState(() {
                        AppData.defaultWorkoutTime = duration;
                      });
                      await AppData.setPreference(
                          "DefaultTime", duration.inSeconds.toString());
                    },
                    autovalidate: true,
                    validator: _validate,
                  )),
              VolumeSetting(
                  name: "Voice",
                  volume: AppData.voiceVolume,
                  onChanged: (newVolume) => setState(() {
                        AppData.voiceVolume = newVolume;
                      })),
              VolumeSetting(
                  name: "Beep",
                  volume: AppData.beepVolume,
                  onChanged: (newVolume) => setState(() {
                        AppData.beepVolume = newVolume;
                      })),
              ListTile(
                leading: Icon(
                  Icons.format_list_bulleted,
                  color: cambridgeColor[900],
                ),
                title: RaisedButton(
                  child: Text("Manage exercises"),
                  onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ExerciseManagePage())),
                ),
              ),
            ],
          ),
        ).toList(),
      ),
    );
  }
}
