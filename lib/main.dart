import 'package:cuoc_circuits_flutter/colours.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:cuoc_circuits_flutter/appdata.dart';
import 'package:cuoc_circuits_flutter/workout_set_time.dart';
import 'package:cuoc_circuits_flutter/settings_drawer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppData.initDatabase();
  runApp(AppEntry());
}

class AppEntry extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CUOC Circuits',
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: cambridgeMaterial,
        accentColor: cuocMaterial,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: cambridgeMaterial,
        accentColor: darkCuocMaterial,
      ),
      home: MainPage(title: 'CUOC Circuits'),
    );
  }
}

class MainPage extends StatefulWidget {
  MainPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MainPageState createState() => _MainPageState();
}

class WorkoutTab extends StatefulWidget {
  @override
  _WorkoutTabState createState() => _WorkoutTabState();
}

GlobalKey<NavigatorState> mainKey = new GlobalKey<NavigatorState>();

class _WorkoutTabState extends State<WorkoutTab>
    with AutomaticKeepAliveClientMixin<WorkoutTab> {
  Navigator navigator = new Navigator(
      key: mainKey,
      onGenerateRoute: (routeSettings) =>
          MaterialPageRoute(builder: (context) => WorkoutTimePage()));

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return navigator;
  }

  @override
  bool get wantKeepAlive => true;
}

class _MainPageState extends State<MainPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => !(await mainKey.currentState.maybePop()),
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          leading: DecoratedBox(
            decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage('assets/cuoc.png')),
            ),
          ),
          title: Text(widget.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.settings,
                color: cuocColor[900],
              ),
              onPressed: () => _scaffoldKey.currentState.openDrawer(),
            )
          ],
        ),
        body: WorkoutTab(),
        drawer: SettingsDrawer(),
      ),
    );
  }
}
